//array of pictures//
var picturesData = [
  {
    imgUrl: 'url(img/1.jpg)',
    imgName: 'Кот первый',
    imgAlt: 'Кот первый'
  },
  {
    imgUrl: 'url(img/2.jpg)',
    imgName: 'Кот второй',
    imgAlt: 'Кот второй'
  },
  {
    imgUrl: 'url(img/2.jpg)',
    imgName: 'Кот второй',
    imgAlt: 'Кот второй'
  },
  {
    imgUrl: 'url(img/2.jpg)',
    imgName: 'Кот второй',
    imgAlt: 'Кот второй'
  },
  {
    imgUrl: 'url(img/3.jpg)',
    imgName: 'Кот третий',
    imgAlt: 'Кот третий'
  }
];
//for creating elements//
var createElem = function (tagName, className, text) {
  var element = document.createElement(tagName);
  element.classList.add(className);
  if (text) {
    element.textContent = text;
  }
  return element
};
//html for slider
var renderPic = function (array) {
  var sliderCont = document.querySelector('.slider-container');

/* ---for swiper?---
  var swipeL = createElem ('div', 'swipeL');
  sliderCont.appendChild(swipeL);
  var swipeR = createElem ('div', 'swipeR');
  sliderCont.appendChild(swipeR);
 */

  var slider = createElem ('div', 'slider');
  sliderCont.appendChild(slider);
  slider = document.querySelector('.slider');
  console.log(slider);
  for (var i = 0; i < array.length; i++) {
    var element = array[i];
    var sliderItem = createElem ('div', 'pic' + (i+1));
    sliderItem.style.backgroundImage = element.imgUrl;
    sliderItem.alt = element.imgAlt;
    sliderItem.classList.add('pic');
    slider.appendChild(sliderItem);
  }
  var buttonCont = createElem ('div', 'button-container');
  document.body.appendChild(buttonCont);
  var buttonPrev = createElem ('button', 'button-prev');
  var shift = 0;
  buttonPrev.textContent = '<<<';
  buttonCont.appendChild(buttonPrev);
  var buttonNext = createElem ('button', 'button-next');
  buttonNext.textContent = '>>>';
	buttonCont.appendChild(buttonNext);
	
	//button actions
  buttonNext.addEventListener('click', function (evtNext) {
    var sliderItems = document.querySelectorAll('.pic');
    var sliderItem = slider.querySelector('.pic1');
    shift -= 380;
    shiftFirst = 380;
    if (shift/-shiftFirst < array.length) {
      sliderItem.style.marginLeft = shift + 'px';
    } else { shift = 0; console.log('cry');}
    sliderItem.style.marginLeft = shift + 'px';
  });  

  buttonPrev.addEventListener('click', function (evtPrev) {
    var sliderItems = document.querySelectorAll('.pic');
    var sliderItem = slider.querySelector('.pic1');
    shift += 380;
    if (shift <= 0) {
      sliderItem.style.marginLeft = shift + 'px';
    } else { shift = -((sliderItems.length-1) * shift); console.log('cry');}
    sliderItem.style.marginLeft = shift + 'px';
	});
	
	//arrow actions
  document.addEventListener('keydown', function (evt) {
    var sliderItems = document.querySelectorAll('.pic');
    var sliderItem = slider.querySelector('.pic1');
    if (evt.keyCode === 39) {
      shift -= 380;
      shiftFirst = 380;
      if (shift/-shiftFirst < array.length) {
        sliderItem.style.marginLeft = shift + 'px';
      } else { shift = 0; console.log('cry');}
      sliderItem.style.marginLeft = shift + 'px';
    }
    else if (evt.keyCode === 37) {
      shift += 380;
      if (shift <= 0) {
        sliderItem.style.marginLeft = shift + 'px';
      } else { shift = -((sliderItems.length-1) * shift); console.log('cry');}
      sliderItem.style.marginLeft = shift + 'px';
    }
  });
};

renderPic(picturesData);
